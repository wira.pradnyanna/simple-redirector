## Getting Started

- Setup docker containers `docker compose up -d`
- Create kafka topic name `links`
- Start web `go run ./cmd/web/main.go`
- Start worker `go run ./cmd/worker/main.go`
- Start grpc server `gor run ./cmd/service/main.go`
- The web listen on port 8080, the grpc server listen on port 9000