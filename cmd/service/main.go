package main

import (
	"net"
	"simple-redirector/internal/config"
	"simple-redirector/internal/delivery/grpc/middleware"
	"simple-redirector/internal/delivery/grpc/server"
	"simple-redirector/internal/delivery/grpc/service"
	"simple-redirector/internal/gateway/messaging"
	"simple-redirector/internal/repository"
	"simple-redirector/internal/usecase"

	"github.com/go-playground/validator/v10"
	"google.golang.org/grpc"
)

const port = ":9000"

func main() {
	log := config.NewLogger()

	validate := validator.New()
	redis := config.NewRedis(log)
	producer := config.NewKafkaProducer(log)
	db := config.NewDatabase(log)

	linkProducer := messaging.NewLinkProducer(producer, log)

	linkRepository := repository.NewLinkRepository(log)

	linkUseCase := usecase.NewLinkUseCase(db, log, validate, redis, linkProducer, linkRepository)
	userUseCase := usecase.NewUserUseCase(log)

	linkServer := server.NewLinkServer(linkUseCase, log)

	authMiddleware := middleware.NewAuth(userUseCase)

	grpcServer := grpc.NewServer(
		grpc.UnaryInterceptor(authMiddleware),
	)
	service.RegisterLinkServer(grpcServer, linkServer)

	log.Sugar().Info("Starting RPC server at", port)

	l, err := net.Listen("tcp", port)
	if err != nil {
		log.Sugar().Errorf("could not listen to %s: %v", port, err)
	}

	log.Sugar().Fatal(grpcServer.Serve(l))
}
