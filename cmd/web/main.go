package main

import (
	"simple-redirector/internal/config"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"go.uber.org/zap"
)

func main() {
	app := gin.Default()
	log := config.NewLogger()
	db := config.NewDatabase(log)
	validate := validator.New()
	producer := config.NewKafkaProducer(log)
	redis := config.NewRedis(log)

	defer log.Sync()

	config.Bootstrap(&config.BootstrapConfig{
		App:      app,
		DB:       db,
		Validate: validate,
		Log:      log,
		Producer: producer,
		Redis:    redis,
	})

	if err := app.Run(); err != nil {
		log.Fatal("Failed to start server", zap.Error(err))
	}
}
