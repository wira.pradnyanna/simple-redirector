package main

import (
	"context"
	"os"
	"os/signal"
	"simple-redirector/internal/config"
	"simple-redirector/internal/delivery/messaging"
	"simple-redirector/internal/repository"
	"syscall"
	"time"
)

func main() {
	log := config.NewLogger()
	db := config.NewDatabase(log)
	accessedLinkRepository := repository.NewLinkAccessedLinkRepository(log)
	redis := config.NewRedis(log)

	log.Info("Starting worker service")

	ctx, cancel := context.WithCancel(context.Background())

	log.Info("Setup link consumer")
	linkConsumer := config.NewKafkaConsumer(log)
	linkHandler := messaging.NewLinkConsumer(db, accessedLinkRepository, redis, log)
	go messaging.ConsumeTopic(ctx, linkConsumer, "links", log, linkHandler.Consume)

	log.Info("Worker is running")

	terminateSignals := make(chan os.Signal, 1)
	signal.Notify(terminateSignals, syscall.SIGINT, syscall.SIGTERM)

	stop := false
	for !stop {
		select {
		case s := <-terminateSignals:
			log.Sugar().Info("Got one of stop signals, shutting down worker gracefully, SIGNAL NAME :", s)
			cancel()
			stop = true
		}
	}

	time.Sleep(5 * time.Second)
}
