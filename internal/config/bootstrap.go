package config

import (
	"simple-redirector/internal/delivery/http"
	"simple-redirector/internal/delivery/http/middleware"
	"simple-redirector/internal/delivery/http/route"
	"simple-redirector/internal/gateway/messaging"
	"simple-redirector/internal/repository"
	"simple-redirector/internal/usecase"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type BootstrapConfig struct {
	DB       *gorm.DB
	App      *gin.Engine
	Log      *zap.Logger
	Validate *validator.Validate
	Producer *kafka.Producer
	Redis    *redis.Client
}

func Bootstrap(config *BootstrapConfig) {
	linkRepository := repository.NewLinkRepository(config.Log)

	linkProducer := messaging.NewLinkProducer(config.Producer, config.Log)

	linkUseCase := usecase.NewLinkUseCase(config.DB, config.Log, config.Validate, config.Redis, linkProducer, linkRepository)
	userUseCase := usecase.NewUserUseCase(config.Log)

	linkController := http.NewLinkController(linkUseCase, config.Log)

	authMiddleware := middleware.NewAuth(userUseCase)

	routeConfig := route.RouteConfig{
		App:            config.App,
		LinkController: linkController,
		AuthMiddleware: authMiddleware,
	}

	routeConfig.Setup()
}
