package config

import (
	"simple-redirector/internal/entity"

	"go.uber.org/zap"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func NewDatabase(log *zap.Logger) *gorm.DB {
	dsn := "host=localhost port=5432 user=root password=root dbname=simple_redirector sslmode=disable"

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Sugar().Fatalf("failed to connect database: %v", err)
	}

	db.AutoMigrate(&entity.Link{})
	db.AutoMigrate(&entity.AccessedLink{})

	return db
}
