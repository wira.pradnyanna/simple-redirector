package config

import (
	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"go.uber.org/zap"
)

const boostrapServer = "localhost:29092"

func NewKafkaConsumer(log *zap.Logger) *kafka.Consumer {
	kafkaConfig := &kafka.ConfigMap{
		"bootstrap.servers": boostrapServer,
		"group.id":          "simple-redirector",
		"auto.offset.reset": "earliest",
	}

	consumer, err := kafka.NewConsumer(kafkaConfig)
	if err != nil {
		log.Sugar().Fatalf("Failed to create consumer: %v", err)
	}
	return consumer
}

func NewKafkaProducer(log *zap.Logger) *kafka.Producer {
	kafkaConfig := &kafka.ConfigMap{
		"bootstrap.servers": boostrapServer,
	}

	producer, err := kafka.NewProducer(kafkaConfig)
	if err != nil {
		log.Sugar().Fatalf("Failed to create producer: %v", err)
	}
	return producer
}
