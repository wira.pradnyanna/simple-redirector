package config

import (
	"os"

	"go.uber.org/zap"
)

func NewLogger() *zap.Logger {
	log := zap.Must(zap.NewProduction())

	if os.Getenv("APP_ENV") == "development" {
		log = zap.Must(zap.NewDevelopment())
	}
	return log
}
