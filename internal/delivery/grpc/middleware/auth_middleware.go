package middleware

import (
	"context"
	"simple-redirector/internal/model"
	"simple-redirector/internal/usecase"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

type AuthContext struct {
	context.Context
	UserId string
}

func NewAuth(userUseCase *usecase.UserUseCase) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Error(codes.Unauthenticated, "missing metadata")
		}
		authorizations := md.Get("authorization")
		if len(authorizations) == 0 {
			return nil, status.Error(codes.Unauthenticated, "missing JWT token")
		}

		authorization := authorizations[0]
		if authorization == "" || !strings.HasPrefix(authorization, "Bearer ") {
			return nil, status.Error(codes.Unauthenticated, "invalid authorization")
		}

		token := strings.TrimPrefix(authorization, "Bearer ")

		request := &model.VerifyUserRequest{
			Token: token,
		}

		auth, err := userUseCase.Verify(ctx, request)
		if err != nil {
			return nil, status.Error(codes.PermissionDenied, "invalid JWT token")
		}

		authContext := &AuthContext{
			Context: ctx,
			UserId:  auth.UserId,
		}

		return handler(authContext, req)
	}
}
