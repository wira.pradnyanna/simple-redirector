package server

import (
	"context"
	"errors"
	"simple-redirector/internal/delivery/grpc/middleware"
	"simple-redirector/internal/delivery/grpc/service"
	"simple-redirector/internal/model"
	"simple-redirector/internal/usecase"

	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type LinkServer struct {
	service.UnimplementedLinkServer
	UseCase *usecase.LinkUseCase
	Log     *zap.Logger
}

func NewLinkServer(useCase *usecase.LinkUseCase, log *zap.Logger) *LinkServer {
	return &LinkServer{
		UseCase: useCase,
		Log:     log,
	}
}

func (s *LinkServer) Create(ctx context.Context, param *service.CreateLinkRequest) (*emptypb.Empty, error) {
	authContext, ok := ctx.(*middleware.AuthContext)
	if !ok {
		return nil, status.Error(codes.InvalidArgument, "failed to access authContext")
	}

	request := &model.CreateLinkRequest{
		Destination: param.Destination,
		UserId:      authContext.UserId,
	}

	if _, err := s.UseCase.Create(ctx, request); err != nil {
		s.Log.Error("failed to create link", zap.Error(err))
		return new(emptypb.Empty), status.Error(codes.InvalidArgument, "failed to create link")
	}

	return new(emptypb.Empty), nil
}

func (s *LinkServer) Edit(ctx context.Context, param *service.EditLinkRequest) (*emptypb.Empty, error) {
	authContext, ok := ctx.(*middleware.AuthContext)
	if !ok {
		return nil, status.Error(codes.InvalidArgument, "failed to access authContext")
	}

	request := &model.UpdateLinkRequest{
		Short:    param.Short,
		NewShort: param.NewShort,
		UserId:   authContext.UserId,
	}

	if _, err := s.UseCase.Update(ctx, request); err != nil {
		s.Log.Error("failed to update link", zap.Error(err))
		return new(emptypb.Empty), status.Error(codes.InvalidArgument, "failed to update link")
	}

	return new(emptypb.Empty), nil
}

func (s *LinkServer) Statistics(ctx context.Context, param *service.GetLinkStatisticRequest) (*service.StatisticsResponse, error) {
	authContext, ok := ctx.(*middleware.AuthContext)
	if !ok {
		return nil, status.Error(codes.InvalidArgument, "failed to access authContext")
	}

	request := &model.GetStatisticsRequest{
		Short:  param.Short,
		UserId: authContext.UserId,
	}

	response, err := s.UseCase.GetStatistic(ctx, request)
	if err != nil {
		if errors.Is(err, usecase.ErrLinkForbidden) {
			s.Log.Error("forbidden to get link statistic", zap.Error(err))
			return nil, status.Error(codes.Unauthenticated, "forbidden to get link statistic")
		} else {
			s.Log.Error("failed to get link statistic", zap.Error(err))
			return nil, status.Error(codes.InvalidArgument, "failed to get link statistic")
		}
	}

	serverResponse := &service.StatisticsResponse{
		TodayCount: uint32(response.TodayCount),
		WeekCount:  uint32(response.WeekCount),
		MonthCount: uint32(response.MonthCount),
		AllCount:   uint32(response.AllCount),
		Short:      request.Short,
	}

	return serverResponse, nil
}
