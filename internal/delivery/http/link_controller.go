package http

import (
	"errors"
	"net/http"
	"simple-redirector/internal/delivery/http/middleware"
	"simple-redirector/internal/model"
	"simple-redirector/internal/usecase"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type LinkController struct {
	UseCase *usecase.LinkUseCase
	Log     *zap.Logger
}

const NOT_FOUND_PAGE_URL = "http://404.com"

func NewLinkController(useCase *usecase.LinkUseCase, log *zap.Logger) *LinkController {
	return &LinkController{
		UseCase: useCase,
		Log:     log,
	}
}

func (c *LinkController) Create(ctx *gin.Context) {
	auth := middleware.GetUser(ctx)

	request := new(model.CreateLinkRequest)

	if err := ctx.ShouldBindJSON(request); err != nil {
		c.Log.Error("failed to parse request body", zap.Error(err))
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	request.UserId = auth.UserId

	response, err := c.UseCase.Create(ctx.Request.Context(), request)
	if err != nil {
		c.Log.Error("failed to create link", zap.Error(err))
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": response,
	})
}

func (c *LinkController) Get(ctx *gin.Context) {
	ipAddress := ctx.ClientIP()
	short, match := ctx.Params.Get("short")

	if !match {
		ctx.Redirect(http.StatusTemporaryRedirect, NOT_FOUND_PAGE_URL)
		return
	}

	request := &model.GetLinkRequest{
		Short:     short,
		IpAddress: ipAddress,
	}

	response, err := c.UseCase.Get(ctx.Request.Context(), request)
	if err != nil {
		c.Log.Error("failed to get link", zap.Error(err))
		ctx.Redirect(http.StatusTemporaryRedirect, NOT_FOUND_PAGE_URL)
		return
	}

	ctx.Redirect(http.StatusPermanentRedirect, response.Destination)
}

func (c *LinkController) Update(ctx *gin.Context) {
	auth := middleware.GetUser(ctx)

	request := new(model.UpdateLinkRequest)

	if err := ctx.ShouldBindJSON(request); err != nil {
		c.Log.Error("failed to parse request body", zap.Error(err))
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	request.UserId = auth.UserId

	response, err := c.UseCase.Update(ctx.Request.Context(), request)
	if err != nil {
		c.Log.Error("failed to update link", zap.Error(err))
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": response,
	})
}

func (c *LinkController) GetStatistics(ctx *gin.Context) {
	auth := middleware.GetUser(ctx)

	short, match := ctx.Params.Get("short")
	if !match {
		c.Log.Error("invalid parameter")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error": "invalid parameter",
		})
		return
	}

	request := &model.GetStatisticsRequest{
		Short:  short,
		UserId: auth.UserId,
	}

	response, err := c.UseCase.GetStatistic(ctx.Request.Context(), request)
	if err != nil {
		if errors.Is(err, usecase.ErrLinkForbidden) {
			c.Log.Error("forbidden to get link statistic", zap.Error(err))
			ctx.AbortWithStatusJSON(http.StatusForbidden, gin.H{
				"error": err.Error(),
			})
			return
		} else {
			c.Log.Error("failed to get link statistic", zap.Error(err))
			ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})
			return
		}
	}

	ctx.JSON(http.StatusOK, gin.H{
		"data": response,
	})
}
