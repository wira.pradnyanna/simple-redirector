package middleware

import (
	"net/http"
	"simple-redirector/internal/model"
	"simple-redirector/internal/usecase"
	"strings"

	"github.com/gin-gonic/gin"
)

func NewAuth(userUseCase *usecase.UserUseCase) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authHeader := ctx.GetHeader("Authorization")
		if authHeader == "" || !strings.HasPrefix(authHeader, "Bearer ") {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": "Unauthorized",
			})
			return
		}

		token := strings.TrimPrefix(authHeader, "Bearer ")

		request := &model.VerifyUserRequest{
			Token: token,
		}

		auth, err := userUseCase.Verify(ctx.Request.Context(), request)

		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": "Unauthorized",
			})
			return
		}

		ctx.Set("auth", auth)
		ctx.Next()
	}
}

func GetUser(ctx *gin.Context) *model.Auth {
	auth, exist := ctx.Get("auth")
	if !exist {
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"error": "Unauthorized",
		})
	}

	return auth.(*model.Auth)
}
