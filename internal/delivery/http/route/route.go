package route

import (
	"simple-redirector/internal/delivery/http"

	"github.com/gin-gonic/gin"
)

type RouteConfig struct {
	App            *gin.Engine
	LinkController *http.LinkController
	AuthMiddleware gin.HandlerFunc
}

func (c *RouteConfig) SetupGuestRoute() {
	c.App.GET("/:short", c.LinkController.Get)
}

func (c *RouteConfig) SetupAuthRoute() {
	c.App.Use(c.AuthMiddleware)
	c.App.POST("/", c.LinkController.Create)
	c.App.PUT("/", c.LinkController.Update)
	c.App.GET("/stats/:short", c.LinkController.GetStatistics)
}

func (c *RouteConfig) Setup() {
	c.SetupGuestRoute()
	c.SetupAuthRoute()
}
