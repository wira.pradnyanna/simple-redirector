package messaging

import (
	"context"
	"time"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"go.uber.org/zap"
)

type ConsumerHandler func(ctx context.Context, message *kafka.Message) error

func ConsumeTopic(ctx context.Context, consumer *kafka.Consumer, topic string, log *zap.Logger, handler ConsumerHandler) {
	err := consumer.Subscribe(topic, nil)
	if err != nil {
		log.Fatal("Failed to subscribe to topic", zap.Error(err))
	}

	run := true

	for run {
		select {
		case <-ctx.Done():
			run = false
		default:
			message, err := consumer.ReadMessage(time.Second)
			if err == nil {
				err := handler(ctx, message)
				if err != nil {
					log.Error("Failed to process message", zap.Error(err))
				} else {
					if _, err = consumer.CommitMessage(message); err != nil {
						log.Fatal("Failed to commit message", zap.Error(err))
					}
				}
			} else if !err.(kafka.Error).IsTimeout() {
				log.Sugar().Warnf("Consumer error: %v (%v)\n", err, message)
			}
		}
	}

	log.Sugar().Infof("Closing consumer for topic : %s", topic)
	err = consumer.Close()
	if err != nil {
		panic(err)
	}
}
