package messaging

import (
	"context"
	"encoding/json"
	"simple-redirector/internal/entity"
	"simple-redirector/internal/model"
	"simple-redirector/internal/repository"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

type LinkConsumer struct {
	Log                    *zap.Logger
	DB                     *gorm.DB
	AccessedLinkRepository *repository.AccessedLinkRepository
	Redis                  *redis.Client
}

func NewLinkConsumer(db *gorm.DB, accessedLinkRepository *repository.AccessedLinkRepository, redis *redis.Client, log *zap.Logger) *LinkConsumer {
	return &LinkConsumer{
		Log:                    log,
		DB:                     db,
		AccessedLinkRepository: accessedLinkRepository,
		Redis:                  redis,
	}
}

func (c LinkConsumer) Consume(ctx context.Context, message *kafka.Message) error {
	linkEvent := new(model.LinkEvent)
	if err := json.Unmarshal(message.Value, linkEvent); err != nil {
		c.Log.Error("error unmarshalling address event", zap.Error(err))
		return err
	}

	c.Log.Sugar().Infof("Received topic addresses with event: %v from partition %d", linkEvent, message.TopicPartition.Partition)

	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	accessedLink := &entity.AccessedLink{
		Short:     linkEvent.Short,
		IpAddress: linkEvent.IpAddress,
	}

	if err := c.AccessedLinkRepository.Create(tx, accessedLink); err != nil {
		c.Log.Error("failed to create accessed link", zap.Error(err))
		return err
	}

	if err := tx.Commit().Error; err != nil {
		c.Log.Error("failed to commit transaction", zap.Error(err))
		return err
	}

	newTx := c.DB.Begin()
	statistic := new(model.LinkStatisticResponse)

	if err := c.AccessedLinkRepository.GetStatisticsByShort(newTx, statistic, linkEvent.Short); err != nil {
		c.Log.Error("failed to get statistic", zap.Error(err))
		return err
	}

	key := "short_" + linkEvent.Short
	value, err := json.Marshal(statistic)
	if err != nil {
		c.Log.Error("failed to marshal statistic", zap.Error(err))
		return err
	}

	if err := c.Redis.Set(ctx, key, value, 0).Err(); err != nil {
		return err
	}

	if err := newTx.Commit().Error; err != nil {
		c.Log.Error("failed to commit new transaction", zap.Error(err))
		return err
	}

	return nil
}
