package entity

import "time"

type AccessedLink struct {
	ID        uint
	Short     string
	IpAddress string
	CreatedAt time.Time
}

func (a *AccessedLink) TableName() string {
	return "accessed_links"
}
