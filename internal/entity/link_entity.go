package entity

type Link struct {
	ID          uint
	Short       string `gorm:"unique"`
	Destination string
	UserId      string
}

func (l *Link) TableName() string {
	return "links"
}
