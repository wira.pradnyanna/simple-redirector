package messaging

import (
	"simple-redirector/internal/model"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"go.uber.org/zap"
)

type LinkProducer struct {
	Producer[*model.LinkEvent]
}

func NewLinkProducer(producer *kafka.Producer, log *zap.Logger) *LinkProducer {
	return &LinkProducer{
		Producer: Producer[*model.LinkEvent]{
			Producer: producer,
			Topic:    "links",
			Log:      log,
		},
	}
}
