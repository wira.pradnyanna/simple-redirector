package messaging

import (
	"encoding/json"
	"simple-redirector/internal/model"

	"github.com/confluentinc/confluent-kafka-go/v2/kafka"
	"go.uber.org/zap"
)

type Producer[T model.Event] struct {
	Producer *kafka.Producer
	Topic    string
	Log      *zap.Logger
}

func (p *Producer[T]) GetTopic() *string {
	return &p.Topic
}

func (p *Producer[T]) Send(event T) error {
	value, err := json.Marshal(event)
	if err != nil {
		p.Log.Error("failed to marshal event", zap.Error(err))
		return err
	}

	message := &kafka.Message{
		TopicPartition: kafka.TopicPartition{
			Topic:     p.GetTopic(),
			Partition: kafka.PartitionAny,
		},
		Value: value,
		Key:   []byte(event.GetId()),
	}

	err = p.Producer.Produce(message, nil)
	if err != nil {
		p.Log.Error("failed to produce message", zap.Error(err))
		return err
	}

	return nil
}
