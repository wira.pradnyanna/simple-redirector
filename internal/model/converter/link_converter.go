package converter

import (
	"simple-redirector/internal/entity"
	"simple-redirector/internal/model"
)

func LinkToResponse(link *entity.Link) *model.LinkResponse {
	return &model.LinkResponse{
		ID:          link.ID,
		Short:       link.Short,
		Destination: link.Destination,
		UserId:      link.UserId,
	}
}
