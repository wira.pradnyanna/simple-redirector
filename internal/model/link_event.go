package model

type LinkEvent struct {
	ID        string `json:"id"`
	Short     string `json:"short"`
	IpAddress string `json:"ip_address"`
}

func (e *LinkEvent) GetId() string {
	return e.ID
}
