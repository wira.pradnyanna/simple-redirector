package model

type LinkResponse struct {
	ID          uint   `json:"id"`
	Short       string `json:"short"`
	Destination string `json:"destination"`
	UserId      string `json:"user_id"`
}

type CreateLinkRequest struct {
	Destination string `json:"destination" validate:"required"`
	UserId      string `json:"-" validate:"required"`
}

type GetLinkRequest struct {
	Short     string `json:"-" validate:"required"`
	IpAddress string `json:"-" validate:"required"`
}

type UpdateLinkRequest struct {
	Short    string `json:"short" validate:"required"`
	NewShort string `json:"new_short" validate:"required"`
	UserId   string `json:"-" validate:"required"`
}

type GetStatisticsRequest struct {
	Short  string `json:"-" validate:"required"`
	UserId string `json:"-" validate:"required"`
}

type LinkStatisticResponse struct {
	TodayCount uint `json:"today_count"`
	WeekCount  uint `json:"week_count"`
	MonthCount uint `json:"month_count"`
	AllCount   uint `json:"all_count"`
}
