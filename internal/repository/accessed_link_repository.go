package repository

import (
	"simple-redirector/internal/entity"
	"simple-redirector/internal/model"

	"go.uber.org/zap"
	"gorm.io/gorm"
)

type AccessedLinkRepository struct {
	Repository[entity.AccessedLink]
	Log *zap.Logger
}

func NewLinkAccessedLinkRepository(log *zap.Logger) *AccessedLinkRepository {
	return &AccessedLinkRepository{
		Log: log,
	}
}

func (r *AccessedLinkRepository) GetStatisticsByShort(db *gorm.DB, statistic *model.LinkStatisticResponse, short string) error {
	return db.Raw(`
		SELECT
		  COUNT(DISTINCT ip_address) AS all_count,
			COUNT(DISTINCT ip_address) FILTER (WHERE created_at::date = CURRENT_DATE) AS day_count,
			COUNT(DISTINCT ip_address) FILTER (WHERE created_at >= date_trunc('week', CURRENT_DATE) AND created_at < date_trunc('week', CURRENT_DATE) + INTERVAL '1 week') AS week_count,
			COUNT(DISTINCT ip_address) FILTER (WHERE created_at >= date_trunc('month', CURRENT_DATE) AND created_at < date_trunc('month', CURRENT_DATE) + INTERVAL '1 month') AS month_count
		FROM
			accessed_links
		WHERE
			short = $1
	`, short).Scan(statistic).Error
}
