package repository

import (
	"simple-redirector/internal/entity"

	"go.uber.org/zap"
	"gorm.io/gorm"
)

type LinkRepository struct {
	Repository[entity.Link]
	Log *zap.Logger
}

func NewLinkRepository(log *zap.Logger) *LinkRepository {
	return &LinkRepository{
		Log: log,
	}
}

func (r *LinkRepository) FindByShort(db *gorm.DB, link *entity.Link, short string) error {
	return db.Where("short = ?", short).Take(link).Error
}

func (r *LinkRepository) FindByShortAndUserId(db *gorm.DB, link *entity.Link, short string, userId string) error {
	return db.Where("short = ? AND user_id = ?", short, userId).Take(link).Error
}
