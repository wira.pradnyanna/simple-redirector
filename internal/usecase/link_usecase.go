package usecase

import (
	"context"
	"encoding/json"
	"errors"
	"math/rand"
	"simple-redirector/internal/entity"
	"simple-redirector/internal/gateway/messaging"
	"simple-redirector/internal/model"
	"simple-redirector/internal/model/converter"
	"simple-redirector/internal/repository"

	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

var ErrLinkForbidden = errors.New("forbidden to access link")

type LinkUseCase struct {
	DB             *gorm.DB
	Log            *zap.Logger
	LinkRepository *repository.LinkRepository
	Validate       *validator.Validate
	LinkProducer   *messaging.LinkProducer
	Redis          *redis.Client
}

func NewLinkUseCase(
	db *gorm.DB,
	log *zap.Logger,
	validate *validator.Validate,
	redis *redis.Client,
	linkProducer *messaging.LinkProducer,
	linkRepository *repository.LinkRepository,
) *LinkUseCase {
	return &LinkUseCase{
		DB:             db,
		Log:            log,
		Validate:       validate,
		LinkRepository: linkRepository,
		LinkProducer:   linkProducer,
		Redis:          redis,
	}
}

func generateShort() string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-"
	var length = rand.Intn(5) + 1
	var uniqueString string

	for i := 0; i < length; i++ {
		var charsetLength = len(charset)

		if i == length-1 {
			charsetLength = len(charset) - 2
		}

		var charsetIndex = rand.Intn(charsetLength)
		uniqueString += string(charset[charsetIndex])
	}

	return uniqueString
}

func (c *LinkUseCase) Create(ctx context.Context, request *model.CreateLinkRequest) (*model.LinkResponse, error) {
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		c.Log.Error("failed to validate request body", zap.Error(err))
		return nil, err
	}

	var short string
	tryCount := 0

	for {
		if tryCount > 5 {
			c.Log.Error("generate short exceed maximum try")
			return nil, errors.New("failed to generate shortened url")
		}

		link := new(entity.Link)
		short = generateShort()

		if err := c.LinkRepository.FindByShort(tx, link, short); errors.Is(err, gorm.ErrRecordNotFound) {
			break
		}
		tryCount++
	}

	link := &entity.Link{
		Short:       short,
		Destination: request.Destination,
		UserId:      request.UserId,
	}

	if err := c.LinkRepository.Create(tx, link); err != nil {
		c.Log.Error("failed to create link", zap.Error(err))
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		c.Log.Error("failed to commit transaction", zap.Error(err))
		return nil, err
	}

	return converter.LinkToResponse(link), nil
}

func (c *LinkUseCase) Get(ctx context.Context, request *model.GetLinkRequest) (*model.LinkResponse, error) {
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	link := new(entity.Link)

	if err := c.LinkRepository.FindByShort(tx, link, request.Short); err != nil {
		c.Log.Error("failed to find link", zap.Error(err))
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		c.Log.Error("failed to commit transaction", zap.Error(err))
		return nil, err
	}

	event := &model.LinkEvent{
		ID:        uuid.New().String(),
		Short:     link.Short,
		IpAddress: request.IpAddress,
	}
	if err := c.LinkProducer.Send(event); err != nil {
		c.Log.Error("failed to publish link event")
		return nil, err
	}

	return converter.LinkToResponse(link), nil
}

func (c *LinkUseCase) Update(ctx context.Context, request *model.UpdateLinkRequest) (*model.LinkResponse, error) {
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	if err := c.Validate.Struct(request); err != nil {
		c.Log.Error("failed to validate request body", zap.Error(err))
		return nil, err
	}

	link := new(entity.Link)

	if err := c.LinkRepository.FindByShortAndUserId(tx, link, request.Short, request.UserId); err != nil {
		c.Log.Error("failed to find link", zap.Error(err))
		return nil, err
	}

	link.Short = request.NewShort

	if err := c.LinkRepository.Update(tx, link); err != nil {
		c.Log.Error("failed to update link", zap.Error(err))
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		c.Log.Error("failed to commit transaction", zap.Error(err))
		return nil, err
	}

	return converter.LinkToResponse(link), nil
}

func (c *LinkUseCase) GetStatistic(ctx context.Context, request *model.GetStatisticsRequest) (*model.LinkStatisticResponse, error) {
	tx := c.DB.WithContext(ctx).Begin()
	defer tx.Rollback()

	link := new(entity.Link)

	if err := c.LinkRepository.FindByShort(tx, link, request.Short); err != nil {
		c.Log.Error("failed to find link", zap.Error(err))
		return nil, err
	}

	if link.UserId != request.UserId {
		c.Log.Error("forbidden to access link statistic")
		return nil, ErrLinkForbidden
	}

	key := "short_" + link.Short
	value, err := c.Redis.Get(ctx, key).Result()
	if err != nil {
		c.Log.Error("failed to get statistic from redis")
		return nil, err
	}

	statistic := new(model.LinkStatisticResponse)
	if err := json.Unmarshal([]byte(value), statistic); err != nil {
		c.Log.Error("failed to unmarshal statistic")
		return nil, err
	}

	if err := tx.Commit().Error; err != nil {
		c.Log.Error("failed to commit transaction", zap.Error(err))
		return nil, err
	}

	return statistic, nil
}
