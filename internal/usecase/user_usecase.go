package usecase

import (
	"context"
	"simple-redirector/internal/model"

	"github.com/golang-jwt/jwt/v5"
	"go.uber.org/zap"
)

const jwtSecret = "01678ba91442ce68270c2cf156ec08045e3a6b22853ba457371a295acda9368d"

type UserUseCase struct {
	Log *zap.Logger
}

func NewUserUseCase(log *zap.Logger) *UserUseCase {
	return &UserUseCase{
		Log: log,
	}
}

func (c *UserUseCase) Verify(ctx context.Context, request *model.VerifyUserRequest) (*model.Auth, error) {
	token, err := jwt.Parse(request.Token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, jwt.ErrSignatureInvalid
		}

		return []byte(jwtSecret), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return nil, jwt.ErrTokenInvalidClaims
	}

	userId, ok := claims["sub"].(string)
	if !ok {
		return nil, jwt.ErrTokenRequiredClaimMissing
	}

	return &model.Auth{
		UserId: userId,
	}, nil
}
