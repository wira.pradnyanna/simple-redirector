package test

import (
	"simple-redirector/internal/config"
	"simple-redirector/internal/gateway/messaging"
	"simple-redirector/internal/repository"
	"simple-redirector/internal/usecase"

	"github.com/go-playground/validator/v10"
)

var linkUseCase *usecase.LinkUseCase

func init() {
	log := config.NewLogger()
	db := config.NewDatabase(log)
	validate := validator.New()
	producer := config.NewKafkaProducer(log)
	redis := config.NewRedis(log)

	linkProducer := messaging.NewLinkProducer(producer, log)
	linkRepository := repository.NewLinkRepository(log)

	linkUseCase = usecase.NewLinkUseCase(db, log, validate, redis, linkProducer, linkRepository)
}
