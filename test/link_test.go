package test

import (
	"context"
	"simple-redirector/internal/model"
	"testing"

	"github.com/stretchr/testify/assert"
)

const testUserId = "mantap"
const testForbiddenUserId = "mantul"
const testLink = "http://test.com"

func TestLinkUseCase(t *testing.T) {
	ctx := context.Background()

	short := testCreate(ctx, t)
	testGet(ctx, t, short)

	newShort := "new_" + short
	testEdit(ctx, t, short, newShort)

	otherShort := testCreate(ctx, t)
	testEditDuplicateError(ctx, t, otherShort, newShort)

	otherShort = testCreate(ctx, t)
	newShort = "new_" + otherShort
	testEditForbiddenError(ctx, t, otherShort, newShort)
}

func testCreate(ctx context.Context, t *testing.T) string {
	request := &model.CreateLinkRequest{
		Destination: testLink,
		UserId:      testUserId,
	}

	response, err := linkUseCase.Create(ctx, request)

	assert.Equal(t, err, nil)
	assert.Equal(t, testLink, response.Destination)
	assert.Equal(t, testUserId, response.UserId)

	return response.Short
}

func testGet(ctx context.Context, t *testing.T, short string) {
	request := &model.GetLinkRequest{
		Short: short,
	}

	_, err := linkUseCase.Get(ctx, request)

	assert.Equal(t, nil, err)
}

func testEdit(ctx context.Context, t *testing.T, short string, newShort string) {
	request := &model.UpdateLinkRequest{
		Short:    short,
		NewShort: newShort,
		UserId:   testUserId,
	}

	response, err := linkUseCase.Update(ctx, request)

	assert.Equal(t, nil, err)
	assert.Equal(t, newShort, response.Short)
}

func testEditDuplicateError(ctx context.Context, t *testing.T, short string, newShort string) {
	request := &model.UpdateLinkRequest{
		Short:    short,
		NewShort: newShort,
		UserId:   testUserId,
	}

	response, err := linkUseCase.Update(ctx, request)

	assert.Equal(t, (*model.LinkResponse)(nil), response)
	assert.NotEqual(t, nil, err)
}

func testEditForbiddenError(ctx context.Context, t *testing.T, short string, newShort string) {
	request := &model.UpdateLinkRequest{
		Short:    short,
		NewShort: newShort,
		UserId:   testForbiddenUserId,
	}

	response, err := linkUseCase.Update(ctx, request)

	assert.Equal(t, (*model.LinkResponse)(nil), response)
	assert.NotEqual(t, nil, err)
}
